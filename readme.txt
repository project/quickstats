OVERVIEW

Quickstats.module is a simple and efficient (from a database point of view) way to count pageviews of nodes.
It is intended for heavy traffic sites where performance is key.

The heart of this module has been coded by Karoly Negyesi (chx) for NowPublic.com. Small improvements and packaging
by Michael Imbeault (fireang3l).

IMPORTANT NOTES:

- Minimum requirement: MySQL 4.1
- This module adds one column (called quickstats_views) to your node table.
- It also has a limit of 100k page views per cron run, controlled by group_concat_max_len in quickstats_cron()
- It will produce a db error if there's no new page views since last cron (which is unlikely on high traffic sites, and not critical if it happens

DOCUMENTATION

Total pageviews are updated every cron to avoid running costly UPDATEs on the potentially big node table.

Two blocks are provided by default, "Popular nodes in the past 7 days" and "Popular nodes in the past 30 days"

This module blocks some spiders (Google, MSN and Yahoo) and Mozilla prefetching requests by default.

Displaying pageviews counts efficiently (should one want to) currently involve patching node.module. A patch file 
(node.patch) is provided. After that, pageviews can be accessed using "print $node->views" or just "print $views"
in node.tpl.php. You can also comment out relevant sections of quickstats.module (see comments before each function).
   
Alternatively, pageviews count can be automatically displayed for users with the appropriate permissions via a
hook_link implementation. This is the default behavior, but it is NOT the recommended one.
Keep in mind that this is inefficient compared to what is described aboved, as it necessitate one SELECT query
per node loaded. This way of doing things is a temporary fix until we can modify fields in node_load programatically.

Uninstallation will clear the quickstats_views table and will drop the quickstats_views column in the node table.